# BCU SDK 0.0.5

This is the bcusdk project from [here](https://sourceforge.net/projects/bcusdk/)
More info about this project can be found [here](https://www.auto.tuwien.ac.at/~mkoegler/index.php/bcusdk)
Clone this repo and follow the guide below.

Build/install pthsem
----
pthsem is needed for bcusdk.

```sh
cd pthsem-2.0.8
./configure
make
sudo make install
```

Build/install bcusdk
----
```ssh
cd ../bcusdk-0.0.5 
export LD_LIBRARY_PATH=/usr/local/lib 
./configure --with-pth=yes --without-pth-test --enable-onlyeibd --enable-eibnetip --enable-eibnetiptunnel --enable-eibnetipserver
make
sudo make install
```

Usage
----
You have to load the dynamic library in /usr/local/lib in order for eibd to work, do the following:
```ssh
echo "/usr/local/lib" | sudo tee -a /etc/ld.so.conf.d/bcusdk.conf
sudo ldconfig
```

Try it out with:
```ssh
sudo route add 224.0.23.12 dev eth0
sudo touch /var/log/eibd.log
sudo chown $USER /var/log/eibd.log
/usr/local/bin/eibd -D -S -T -i --eibaddr=0.0.1 --daemon=/var/log/eibd.log --no-tunnel-client-queuing ipt:192.168.10.10
/usr/local/bin/groupsocketlisten ip:127.0.0.1
```
(change the ip number 192.168.10.10 to the ip number of your knx gateway) 
